<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-37761633-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-37761633-2');
</script>


## User-Manager

if you are looking for a simple user manager system, so you can use it in your own app. this is for you.
## Main Features
<ul>
	<li><b>Fully Customizable</b>: There is an installation script that you can configure Website Title, domain, email(that is used while sending emails) AND user properties in the way you want</li>
<img src="https://raw.githubusercontent.com/irhosseinz/User-Manager/master/install/screen_shots/install.png" style="border:3px solid black"/>
<img src="https://raw.githubusercontent.com/irhosseinz/User-Manager/master/install/screen_shots/install_fields.png" style="border:3px solid black"/>
	<li><b>Registration</b>: Simple Registeration With jquery form Validation</li>
<img src="https://raw.githubusercontent.com/irhosseinz/User-Manager/master/install/screen_shots/register.png" style="border:3px solid black"/>
	<li><b>Login</b>: User can login and its data is saved in $_SESSION['UM_DATA']. you can use it anywhere you want!<br/>+ There is `Remember Me` option in login. user can select it so he will stay online for 10 days (you can change this by changing `UM_LOGIN_EXPIRE` in config.php)</li>
	<li><b>Dashboard</b>: dashboard contains a profile manager that user can edit his profile data, But you can add other sections to it. I've used <a href="https://feathericons.com/" target="_blank">Feather Icons</a> for icons. you can use them easily</li>
<img src="https://raw.githubusercontent.com/irhosseinz/User-Manager/master/install/screen_shots/dashboard.png" style="border:3px solid black"/>
	<li><b>Email Verification</b></li>
	<li><b>Password Reset</b>: User can reset his/her password in case of forgeting that</li>
	<li><b>Secure</b>: All security measures are observed</li>
	<li><b>Recaptcha Support</b>: You can get <a href="https://www.google.com/recaptcha/admin" target="_blank">Recaptcha V3</a> api Keys and enter it while Installation, so recaptcha will be used in <u>background</u> on login, register and password reset request.</li>
</ul>

## Special Thanks
  [@thibaut-decherit](https://github.com/thibaut-decherit) - For security notes

## Show your support
Give a ⭐️ if this project helped you!
